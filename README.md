<h1 align="center"><code> 🕑:SN-DGN2-2</code></h1>
<h2 align="center"><i> Continue - SN-DG2 </i></h2>

----
1. [What ?](#what-)
2. [Main Series](#main-series)
   1. [Official Repo](#official-repo)
3. [Commands](#commands)
   1. [Official PNPM method](#official-pnpm-method)
   2. [New Docs](#new-docs)
4. [Branches](#branches)

----

# What ? 

Contiuation of work from 
[`https://gitlab.com/sniffowomo/sn-dn13b`](https://gitlab.com/sniffowomo/sn-dn13b)

Continuation of work from 
[`https://gitlab.com/sniffowomo/sn-dgn2`](https://gitlab.com/sniffowomo/sn-dgn2)
- Done because disovering Api building wiht NJ 
- 

# Main Series 

[`https://youtube.com/playlist?list=PL0Zuz27SZ-6Pk-QJIdGd1tGZEzy9RTgtj`](https://youtube.com/playlist?list=PL0Zuz27SZ-6Pk-QJIdGd1tGZEzy9RTgtj)
- DG NextJs Learning Series 

## Official Repo 

[`https://github.com/gitdagray/next-js-course`](https://github.com/gitdagray/next-js-course)
- Main work Repo 

# Commands 

```ml 
pnpx create-next-app@latest
```

- Note this has taiwind CSS in the default installtion 
- As of writing this NextJS have made some changes 
- You can choose tyepscript and tailwind here

## Official PNPM method 

As stated in the docs use pnpm like this to start a new app

```sh 

```

## New Docs 

[`https://nextjs.org/docs`](https://nextjs.org/docs)
- Changed as of `Fri May  5 10:24:01 AM UTC 2023`

 # Branches 

 Work segregation via branches as usual 

 